﻿using Microsoft.DirectX.Direct3D;
using System;
using System.Collections.Generic;

namespace Sentinel.Render.VertexDefinitions
{
    public abstract class VertexDefinition
    {
        public abstract VertexDeclaration GetDeclaration(Device device);
        public abstract Dictionary<int, Type> GetStreamTypes();

        public virtual VertexFormats GetStreamFormat(int streamIndex) =>
            throw new IndexOutOfRangeException(streamIndex.ToString());

        public static VertexDefinition Get(Geometry.VertexType vertexType) =>
            Activator.CreateInstance(VertexTypes[vertexType]) as VertexDefinition;

        public static VertexDefinition Get(Geometry.PrtType prtType) =>
            Activator.CreateInstance(PrtTypes[prtType]) as VertexDefinition;

        public static Dictionary<Geometry.VertexType, Type> VertexTypes { get; } = new Dictionary<Geometry.VertexType, Type>
        {
            [Geometry.VertexType.World] = typeof(WorldVertexDefinition),
            [Geometry.VertexType.Rigid] = typeof(RigidVertexDefinition),
            [Geometry.VertexType.Skinned] = typeof(SkinnedVertexDefinition),
            [Geometry.VertexType.ParticleModel] = typeof(ParticleModelVertexDefinition),
            [Geometry.VertexType.FlatWorld] = typeof(FlatWorldVertexDefinition),
            [Geometry.VertexType.FlatRigid] = typeof(FlatRigidVertexDefinition),
            [Geometry.VertexType.FlatSkinned] = typeof(FlatSkinnedVertexDefinition),
            [Geometry.VertexType.Screen] = typeof(ScreenVertexDefinition),
            [Geometry.VertexType.Debug] = typeof(DebugVertexDefinition),
            [Geometry.VertexType.Transparent] = typeof(TransparentVertexDefinition),
            [Geometry.VertexType.Particle] = typeof(ParticleVertexDefinition),
            [Geometry.VertexType.Contrail] = typeof(ContrailVertexDefinition),
            [Geometry.VertexType.LightVolume] = typeof(LightVolumeVertexDefinition),
            [Geometry.VertexType.ChudSimple] = typeof(ChudVertexSimpleDefinition),
            [Geometry.VertexType.ChudFancy] = typeof(ChudVertexFancyDefinition),
            [Geometry.VertexType.Decorator] = typeof(DecoratorVertexDefinition),
            [Geometry.VertexType.TinyPosition] = typeof(TinyPositionVertexDefinition),
            [Geometry.VertexType.PatchyFog] = typeof(PatchyFogVertexDefinition),
            [Geometry.VertexType.Water] = typeof(WaterVertexDefinition),
            [Geometry.VertexType.Ripple] = typeof(RippleVertexDefinition),
            [Geometry.VertexType.Implicit] = typeof(ImplicitVertexDefinition),
            [Geometry.VertexType.Beam] = typeof(BeamVertexDefinition),
            [Geometry.VertexType.DualQuat] = typeof(DualQuatVertexDefinition)
        };

        public static Dictionary<Geometry.PrtType, Type> PrtTypes { get; } = new Dictionary<Geometry.PrtType, Type>
        {
            [Geometry.PrtType.None] = typeof(StaticShDefinition),
            [Geometry.PrtType.Ambient] = typeof(StaticPrtAmbientDefinition),
            [Geometry.PrtType.Linear] = typeof(StaticPrtLinearDefinition),
            [Geometry.PrtType.Quadratic] = typeof(StaticPrtQuadraticDefinition)
        };
    }
}