using Sentinel.Serialization;

namespace Sentinel.Tags.Definitions
{
    [TagStructure(Name = "shader_decal", Tag = "rmd ", Size = 0x0)]
    public class ShaderDecal : RenderMethod
    {
    }
}