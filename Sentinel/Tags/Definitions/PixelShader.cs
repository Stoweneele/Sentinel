using Sentinel.Serialization;
using Sentinel.Shaders;
using System.Collections.Generic;

namespace Sentinel.Tags.Definitions
{
    [TagStructure(Name = "pixel_shader", Tag = "pixl", Size = 0x20)]
    public class PixelShader
    {
        public uint Unknown;
        public List<ShaderDrawMode> DrawModes;
        public uint Unknown3;
        public List<PixelShaderBlock> Shaders;
    }
}