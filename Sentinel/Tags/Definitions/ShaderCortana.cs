using Sentinel.Serialization;

namespace Sentinel.Tags.Definitions
{
    [TagStructure(Name = "shader_cortana", Tag = "rmct", Size = 0x4)]
    public class ShaderCortana : RenderMethod
    {
        public int Unknown8;    
    }
}