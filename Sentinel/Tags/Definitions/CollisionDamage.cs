using Sentinel.Cache;
using Sentinel.Common;
using Sentinel.Serialization;

namespace Sentinel.Tags.Definitions
{
    [TagStructure(Name = "collision_damage", Tag = "cddf", Size = 0x28, MaxVersion = CacheVersion.Halo3ODST)]
    [TagStructure(Name = "collision_damage", Tag = "cddf", Size = 0x30, MinVersion = CacheVersion.HaloOnline106708)]
    public class CollisionDamage
    {
        public float ApplyDamageScale;
        public float ApplyRecoilDamageScale;
        public Bounds<float> DamageAccelerationBounds;
        public Bounds<float> DamageScaleBounds;
        [TagField(MinVersion = CacheVersion.HaloOnline106708)]
        public Bounds<float> Unknown;
        public Bounds<float> RecoilDamageAccelerationBounds;
        public Bounds<float> RecoilDamageScaleBounds;
    }
}
