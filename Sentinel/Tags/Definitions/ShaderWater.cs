using Sentinel.Serialization;

namespace Sentinel.Tags.Definitions
{
    [TagStructure(Name = "shader_water", Tag = "rmw ", Size = 0x0)]
    public class ShaderWater : RenderMethod
    {
    }
}
