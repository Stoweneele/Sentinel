using Sentinel.Cache;
using Sentinel.Common;
using Sentinel.Serialization;
using System;
using System.Collections.Generic;

namespace Sentinel.Tags.Definitions
{
    [TagStructure(Name = "render_method_template", Tag = "rmt2", Size = 0x84, MaxVersion = CacheVersion.Halo3ODST)]
    [TagStructure(Name = "render_method_template", Tag = "rmt2", Size = 0x90, MinVersion = CacheVersion.HaloOnline106708)]
    public class RenderMethodTemplate
    {
        public CachedTagInstance VertexShader;
        public CachedTagInstance PixelShader;
        public ShaderModeFlags ShaderModes;
        public List<DrawMode> DrawModes; // Entries in here correspond to an enum in the EXE
        public List<DrawModeRegisterOffsetBlock> DrawModeRegisterOffsets;
        public List<ArgumentMapping> ArgumentMappings;
        public List<ShaderArgument> Arguments;
        public List<ShaderArgument> Unknown5;
        public List<ShaderArgument> GlobalArguments;
        public List<ShaderArgument> ShaderMaps;
        public uint Unknown7;
        public uint Unknown8;
        public uint Unknown9;

        [TagField(Padding = true, Length = 12, MinVersion = CacheVersion.HaloOnline106708)]
        public byte[] Unused;

        public enum ShaderMode : sbyte
        {
            Default,
            Albedo,
            StaticDefault,
            StaticPerPixel,
            StaticPerVertex,
            StaticSh,
            StaticPrtAmbient,
            StaticPrtLinear,
            StaticPrtQuadratic,
            DynamicLight,
            ShadowGenerate,
            ShadowApply,
            ActiveCamo,
            LightmapDebugMode,
            StaticPerVertexColor,
            WaterTessellation,
            WaterShading,
            DynamicLightCinematic,
            ZOnly,
            SfxDistort
        }

        [Flags]
        public enum ShaderModeFlags : uint
        {
            Default = 1 << 0,
            Albedo = 1 << 1,
            StaticDefault = 1 << 2,
            StaticPerPixel = 1 << 3,
            StaticPerVertex = 1 << 4,
            StaticSh = 1 << 5,
            StaticPrtAmbient = 1 << 6,
            StaticPrtLinear = 1 << 7,
            StaticPrtQuadratic = 1 << 8,
            DynamicLight = 1 << 9,
            ShadowGenerate = 1 << 10,
            ShadowApply = 1 << 11,
            ActiveCamo = 1 << 12,
            LightmapDebugMode = 1 << 13,
            StaticPerVertexColor = 1 << 14,
            WaterTessellation = 1 << 15,
            WaterShading = 1 << 16,
            DynamicLightCinematic = 1 << 17,
            ZOnly = 1 << 18,
            SfxDistort = 1 << 19,
        }

        [TagStructure(Size = 0x2)]
        public class DrawMode
        {
            public ShaderMode PixelShaderMode;
            public ShaderMode VertexShaderMode;
        }

        [TagStructure(Size = 0x1C)]
        public class DrawModeRegisterOffsetBlock
        {
            [TagField(Length = 14)]
            public ushort[] RegisterMappings;

            public enum DrawModeRegisterOffsetType
            {
                ShaderMapSamplerRegisters,
                UnknownVectorRegisters,
                Unknown1,
                Unknown2,
                ArgumentsVectorRegisters,
                Unknown3,
                GlobalArgumentsVectorRegisters,
                RenderBufferSamplerRegisters,
                Unknown4,
                Unknown5,
                DebugVectorRegisters,
                Unknown6,
                Unknown7,
                Unknown8,
                DrawModeRegisterOffsetType_Count
            }

            [Flags]
            public enum DrawModeRegisterOffsetTypeFlags
            {
                ShaderMapSamplerRegisters = 1 << 0,
                UnknownVectorRegisters = 1 << 1,
                Unknown1 = 1 << 2,
                Unknown2 = 1 << 3,
                ArgumentsVectorRegisters = 1 << 4,
                Unknown3 = 1 << 5,
                GlobalArgumentsVectorRegisters = 1 << 6,
                RenderBufferSamplerRegisters = 1 << 7,
                Unknown4 = 1 << 8,
                Unknown5 = 1 << 9,
                DebugVectorRegisters = 1 << 10,
                Unknown6 = 1 << 11,
                Unknown7 = 1 << 12,
                Unknown8 = 1 << 13
            }

            public ushort GetCount(DrawModeRegisterOffsetType offset) => (ushort)(RegisterMappings[(int)offset] >> 10);
            public ushort GetOffset(DrawModeRegisterOffsetType offset) => (ushort)(RegisterMappings[(int)offset] & 0x3FFu);

            public void SetCount(DrawModeRegisterOffsetType offset, ushort count)
            {
                if (count > 0x3Fu) throw new System.Exception("Out of range");
                var value = (ushort) (GetOffset(offset) & ((count & 0x3F) << 10));
                RegisterMappings[(int)offset] = value;
            }

            public void SetOffset(DrawModeRegisterOffsetType offset, ushort _offset)
            {
                if (_offset > 0x3FFu) throw new System.Exception("Out of range");
                var value = (ushort)((_offset & 0x3FF) & ((GetCount(offset) & 0x3F) << 10));
                RegisterMappings[(int)offset] = value;
            }
        }

        /// <summary>
        /// Binds an argument in the render method tag to a pixel shader constant.
        /// </summary>
        [TagStructure(Size = 0x4)]
        public class ArgumentMapping
        {
            /// <summary>
            /// The GPU register to bind the argument to.
            /// </summary>
            public ushort RegisterIndex;

            /// <summary>
            /// The index of the argument in one of the blocks in the render method tag.
            /// The block used depends on the argument type.
            /// </summary>
            public byte ArgumentIndex;

            public byte Unknown;
        }

        [TagStructure(Size = 0x4)]
        public class ShaderArgument
        {
            [TagField(Label = true)]
            public StringId Name;
        }
    }
}