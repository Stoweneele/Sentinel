using Sentinel.Cache;
using Sentinel.Serialization;
using Sentinel.Shaders;
using System.Collections.Generic;

namespace Sentinel.Tags.Definitions
{
    [TagStructure(Name = "vertex_shader", Tag = "vtsh", Size = 0x20, MinVersion = CacheVersion.Halo3Retail)]
    public class VertexShader
    {
        public uint Unknown;
        public List<DrawModeList> DrawModeLists;
        public uint Unknown3;
        public List<VertexShaderBlock> Shaders;

        [TagStructure(Size = 0xC)]
        public class DrawModeList
        {
            public List<ShaderDrawMode> DrawModes;
        }
    }
}