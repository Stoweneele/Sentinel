﻿using Sentinel.Cache;
using Sentinel.Serialization;

namespace Sentinel.Common
{
    [TagStructure(Size = 0x10)]
    public struct TagReferenceBlock
    {
        [TagField(Label = true)]
        public CachedTagInstance Instance;
    }
}