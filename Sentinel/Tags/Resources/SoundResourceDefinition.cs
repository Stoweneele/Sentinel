using Sentinel.Serialization;

namespace Sentinel.Tags.Resources
{
    [TagStructure(Name = "sound_resource_definition", Size = 0x14)]
    public class SoundResourceDefinition
    {
        public TagData Data;
    }
}