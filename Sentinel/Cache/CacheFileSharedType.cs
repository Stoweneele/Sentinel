﻿namespace Sentinel.Cache
{
    public enum CacheFileSharedType : short
    {
        None = -1,
        MainMenu,
        Shared,
        Campaign
    }
}