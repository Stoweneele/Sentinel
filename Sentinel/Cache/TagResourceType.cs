﻿namespace Sentinel.Cache
{
    public enum TagResourceType : sbyte
    {
        None = -1,
        Collision,
        Bitmap,
        BitmapInterleaved,
        Sound,
        Animation,
        RenderGeometry,
        Bink,
        Pathfinding
    }
}