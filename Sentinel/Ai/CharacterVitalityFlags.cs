using System;

namespace Sentinel.Ai
{
    [Flags]
    public enum CharacterVitalityFlags : int
    {
        None = 0,
        AutoResurrect = 1 << 0
    }
}
