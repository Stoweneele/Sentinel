using Sentinel.Common;
using Sentinel.Serialization;
using System.Collections.Generic;

namespace Sentinel.Ai
{
    [TagStructure(Size = 0x14)]
    public class CharacterVariant
    {
        [TagField(Label = true)]
        public StringId VariantName;
        public short VariantIndex;

        [TagField(Padding = true, Length = 2)]
        public byte[] Unused;

        public List<CharacterDialogueVariation> DialogueVariations;
    }
}
