using Sentinel.Common;
using Sentinel.Serialization;

namespace Sentinel.Ai
{
    [TagStructure(Size = 0x8)]
    public class CharacterChangeDirectionPause
    {
        public Angle DirectionChangeAngle;
        public int StationaryChange;
    }
}
