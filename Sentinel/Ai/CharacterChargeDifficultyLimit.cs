using Sentinel.Serialization;

namespace Sentinel.Ai
{
    [TagStructure(Size = 0x6)]
    public class CharacterChargeDifficultyLimit
    {
        public short MaximumKamikazeCount;
        public short MaximumBerserkCount;
        public short MinimumBerserkCount;
    }
}
