using Sentinel.Cache;
using Sentinel.Common;
using Sentinel.Serialization;

namespace Sentinel.Ai
{
    [TagStructure(Size = 0x58)]
    public class CharacterRetreatProperties
    {
        public CharacterRetreatFlags Flags;
        public float ShieldThreshold;
        public float ScaryTargetThreshold;
        public float DangerThreshold;
        public float ProximityThreshold;
        public Bounds<float> ForcedCowerTimeBounds;
        public Bounds<float> CowerTimeBounds;
        public float ProximityAmbushThreshold;
        public float AwarenessAmbushThreshold;
        public float LeaderDeadRetreatChance;
        public float PeerDeadRetreatChance;
        public float SecondPeerDeadRetreatChance;
        public float FleeTimeout;
        public Angle ZigZagAngle;
        public float ZigZagPeriod;
        public float RetreatGrenadeChance;
        public CachedTagInstance BackupWeapon;
    }
}
