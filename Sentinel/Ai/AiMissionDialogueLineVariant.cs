using Sentinel.Cache;
using Sentinel.Common;
using Sentinel.Serialization;

namespace Sentinel.Ai
{
    [TagStructure(Size = 0x18)]
    public class AiMissionDialogueLineVariant
    {
        [TagField(Label = true)]
        public StringId Designation;
        public CachedTagInstance Sound;
        public StringId SoundEffect;
    }
}
