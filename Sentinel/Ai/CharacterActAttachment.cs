using Sentinel.Cache;
using Sentinel.Common;
using Sentinel.Serialization;

namespace Sentinel.Ai
{
    [TagStructure(Size = 0x1C)]
    public class CharacterActAttachment
    {
        [TagField(Label = true)]
        public StringId Name;
        public CachedTagInstance ChildObject;
        public StringId ChildMarker;
        public StringId ParentMarker;
    }
}
