using System;

namespace Sentinel.Ai
{
    [Flags]
    public enum CharacterPreSearchFlags : int
    {
        None = 0,
        Flag1 = 1 << 0
    }
}
