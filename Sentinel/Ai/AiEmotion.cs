namespace Sentinel.Ai
{
    public enum AiEmotion : short
    {
        None,
        Happy,
        Sad,
        Angry,
        Disgusted,
        Scared,
        Surprised,
        Pain,
        Shout
    }
}
