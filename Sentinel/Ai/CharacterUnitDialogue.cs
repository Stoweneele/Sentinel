using Sentinel.Serialization;
using System.Collections.Generic;

namespace Sentinel.Ai
{
    [TagStructure(Size = 0xC)]
    public class CharacterUnitDialogue
    {
        public List<CharacterDialogueVariation> DialogueVariations;
    }
}
