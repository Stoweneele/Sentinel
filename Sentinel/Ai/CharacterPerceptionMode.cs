namespace Sentinel.Ai
{
    public enum CharacterPerceptionMode : short
    {
        Idle,
        Alert,
        Combat,
        Search,
        Patrol,
        VehicleIdle,
        VehicleAlert,
        VehicleCombat
    }
}
