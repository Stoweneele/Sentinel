namespace Sentinel.Ai
{
    public enum CharacterWeaponSpecialFireSituation : short
    {
        Never,
        EnemyVisible,
        EnemyOutOfSight,
        Strafing
    }
}
