using Sentinel.Cache;
using Sentinel.Common;
using Sentinel.Serialization;

namespace Sentinel.Ai
{
    [TagStructure(Size = 0x18)]
    public class CharacterDialogueVariation
    {
        public CachedTagInstance Dialogue;
        [TagField(Label = true)]
        public StringId Name;
        public float Weight;
    }
}
