using Sentinel.Cache;
using Sentinel.Serialization;
using System.Collections.Generic;

namespace Sentinel.Ai
{
    [TagStructure(Size = 0x24)]
    public class CharacterEquipmentProperties
    {
        [TagField(Label = true)]
        public CachedTagInstance Equipment;
        public uint Unknown;
        public float UsageChance;
        public List<CharacterEquipmentUsageCondition> UsageConditions;
    }
}
