namespace Sentinel.Ai
{
    public enum AiDialoguePerception : short
    {
        None,
        Speaker,
        Listener
    }
}
