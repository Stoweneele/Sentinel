namespace Sentinel.Ai
{
    public enum CharacterGrenadeTrajectoryType : short
    {
        Toss,
        Lob,
        Bounce
    }
}
