using Sentinel.Cache;
using Sentinel.Serialization;
using System.Collections.Generic;

namespace Sentinel.Ai
{
    [TagStructure(Size = 0x1C)]
    public class CharacterFiringPatternProperties
    {
        [TagField(Label = true)]
        public CachedTagInstance Weapon;
        public List<CharacterFiringPattern> FiringPatterns;
    }
}
