using Sentinel.Common;
using Sentinel.Serialization;
using System.Collections.Generic;

namespace Sentinel.Ai
{
    [TagStructure(Size = 0x14)]
    public class AiMissionDialogueLine
    {
        [TagField(Label = true)]
        public StringId Name;
        public List<AiMissionDialogueLineVariant> Variants;
        public StringId DefaultSoundEffect;
    }
}
