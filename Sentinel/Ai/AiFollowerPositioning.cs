namespace Sentinel.Ai
{
    public enum AiFollowerPositioning : short
    {
        InFrontOfMe,
        BehindMe,
        Tight
    }
}
