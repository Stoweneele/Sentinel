namespace Sentinel.Ai
{
    public enum CharacterJumpHeight : int
    {
        None,
        Down,
        Step,
        Crouch,
        Stand,
        Storey,
        Tower,
        Infinite
    }
}
