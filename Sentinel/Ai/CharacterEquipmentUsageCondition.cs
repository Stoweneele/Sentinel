using Sentinel.Serialization;

namespace Sentinel.Ai
{
    [TagStructure(Size = 0xC)]
    public class CharacterEquipmentUsageCondition
    {
        public short Unknown;
        public short Unknown2;
        public uint Unknown3;
        public uint Unknown4;
    }
}
