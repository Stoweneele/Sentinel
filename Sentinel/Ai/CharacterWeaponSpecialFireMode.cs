namespace Sentinel.Ai
{
    public enum CharacterWeaponSpecialFireMode : short
    {
        None,
        Overcharge,
        SecondaryTrigger
    }
}
