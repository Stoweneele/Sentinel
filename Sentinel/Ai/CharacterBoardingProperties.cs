using Sentinel.Serialization;

namespace Sentinel.Ai
{
    [TagStructure(Size = 0x14)]
    public class CharacterBoardingProperties
    {
        public CharacterBoardingFlags Flags;
        public float MaximumDistance;
        public float AbortDistance;
        public float MaximumSpeed;
        public float BoardTime;
    }
}
