namespace Sentinel.Ai
{
    public enum CharacterDefaultGrenadeType : short
    {
        None,
        HumanFragmentation,
        CovenantPlasma,
        BruteClaymore,
        Firebomb
    }
}