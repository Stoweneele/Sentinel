﻿using System;

namespace Sentinel.Ai
{
    [Flags]
    public enum CharacterMetagameFlags : byte
    {
        None,
        MustHaveActiveSeats = 1 << 0
    }
}