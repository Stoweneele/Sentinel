namespace Sentinel.IO
{
    public enum EndianFormat
    {
        LittleEndian,
        BigEndian
    }
}
