﻿namespace Sentinel.Geometry
{
    public enum RenderGeometryClassification : short
    {
        Worldspace,
        Rigid,
        RigidBoned,
        Skinned,
        Unsupported
    }
}