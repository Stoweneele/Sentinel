namespace Sentinel.Geometry
{
    /// <summary>
    /// Vertex buffer formats.
    /// </summary>
    public enum VertexBufferFormat : short
    {
        Invalid,
        World,                // Size = 0x38
        Rigid,                // Size = 0x38
        Skinned,              // Size = 0x40
        StaticPerPixel,       // Size = 0x8
        Unknown5,             // Size = 0x4
        StaticPerVertex,      // Size = 0x14
        Unknown7,             // Size = 0x14

        Unused1, 

        AmbientPrt,           // Size = 0x4
        LinearPrt,            // Size = 0x4
        QuadraticPrt,         // Size = 0x24
        UnknownC,             // Size = 0x14
        UnknownD,             // Size = 0x10
        StaticPerVertexColor, // Size = 0xC
        PatchyFogStream0,     // Size = 0x18

        Unused2,
        Unused3,
        Unused4,
        Unused5,

        TinyPosition,         // Size = 0x8
        Unknown15,            // Size = 0x4
        Unknown16,            // Size = 0x4
        Unknown17,            // Size = 0x4
        DecoratorStream0,     // Size = 0x20
        ParticleModel,        // Size = 0x20
        WaterStream2,         // Size = 0xC
        WaterStream3,         // Size = 0x24
        WaterStream1,         // Size = 0x9C

        Unused6,

        World2                // Size = 0x38 (1.235640+ only)
    }
}
