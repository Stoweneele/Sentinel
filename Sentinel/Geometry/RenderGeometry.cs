using Sentinel.Cache;
using Sentinel.Common;
using Sentinel.Serialization;
using System;
using System.Collections.Generic;

namespace Sentinel.Geometry
{
    [TagStructure(Name = "render_geometry", Size = 0x84)]
    public class RenderGeometry
    {
        /// <summary>
        /// The runtime flags of the render geometry.
        /// </summary>
        public RenderGeometryRuntimeFlags RuntimeFlags;

        /// <summary>
        /// The meshes of the render geometry.
        /// </summary>
        public List<Mesh> Meshes;

        /// <summary>
        /// The compression information of the render geometry.
        /// </summary>
        public List<RenderGeometryCompression> Compression;

        /// <summary>
        /// The bounding spheres of the render geometry.
        /// </summary>
        public List<BoundingSphere> BoundingSpheres;

        public List<UnknownBlock> Unknown2;

        public uint Unknown3;
        public uint Unknown4;
        public uint Unknown5;

        public List<UnknownSection> UnknownSections;

        /// <summary>
        /// The per-mesh node mappings of the render geometry.
        /// </summary>
        public List<PerMeshNodeMap> PerMeshNodeMaps;

        /// <summary>
        /// The per-mesh subpart visibility of the render geometry.
        /// </summary>
        public List<PerMeshSubpartVisibilityBlock> PerMeshSubpartVisibility;

        public uint Unknown7;
        public uint Unknown8;
        public uint Unknown9;

        /// <summary>
        /// The per-mesh level-of-detail data of the render geometry.
        /// </summary>
        public List<PerMeshLodDatum> PerMeshLodData;
        
        /// <summary>
        /// The resource containing the raw geometry data.
        /// </summary>
        [TagField(Pointer = true, MinVersion = CacheVersion.HaloOnline106708)]
        public PageableResource Resource;

        /// <summary>
        /// The index of the resource entry in the cache_file_resource_gestalt tag.
        /// </summary>
        [TagField(MaxVersion = CacheVersion.Halo3ODST)]
        public int ZoneAssetHandle;
        
        [TagField(Padding = true, Length = 4)]
        public byte[] Unused;

        /// <summary>
        /// Mesh flags.
        /// </summary>
        [Flags]
        public enum MeshFlags : byte
        {
            None = 0,

            /// <summary>
            /// Indicates that the mesh has vertex colors instead of PRT data.
            /// </summary>
            MeshHasVertexColor = 1 << 0,

            UseRegionIndexForSorting = 1 << 1,
            CanBeRenderedInDrawBundles = 1 << 2,
            MeshIsCustomShadowCaster = 1 << 3,
            MeshIsUnindexed = 1 << 4,
            MashShouldRenderInZPrepass = 1 << 5,
            MeshHasWater = 1 << 6,
            MeshHasDecal = 1 << 7
        }

        /// <summary>
        /// A 3D mesh which can be rendered.
        /// </summary>
        [TagStructure(Size = 0xB4, MaxVersion = CacheVersion.Halo2Vista)]
        [TagStructure(Size = 0x4C, MinVersion = CacheVersion.Halo3Retail)]
        public class Mesh
        {
            public List<Part> Parts;
            public List<SubPart> SubParts;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public List<VisibilityBinding> VisibilityBounds;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public List<RawVertex> RawVertices;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public List<StripIndex> StripIndices;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public byte[] VisibilityMoppCodeData;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public List<StripIndex> MoppReorderTable;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public List<VertexBuffer> VertexBuffers;

            [TagField(Padding = true, Length = 4, MaxVersion = CacheVersion.Halo2Vista)]
            public byte[] Unused1;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public List<RawPoint> RawPoints;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public byte[] RuntimePointData;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public List<RigidPointGroup> RigidPointGroups;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public List<PointDataIndex> VertexPointIndices;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public List<NodeMapping> NodeMap;

            [TagField(Padding = true, Length = 4, MaxVersion = CacheVersion.Halo2Vista)]
            public byte[] Unused2 = new byte[4];

            [TagField(Length = 8, MinVersion = CacheVersion.Halo3Retail)]
            public ushort[] VertexBufferIndices;

            [TagField(Length = 2, MinVersion = CacheVersion.Halo3Retail)]
            public ushort[] IndexBufferIndices;

            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public MeshFlags Flags;

            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public sbyte RigidNodeIndex;

            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public VertexType Type;

            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public PrtType PrtType;

            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public PrimitiveType IndexBufferType;

            [TagField(Padding = true, Length = 3, MinVersion = CacheVersion.Halo3Retail)]
            public byte[] Unused3;

            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public List<InstancedGeometryBlock> InstancedGeometry;

            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public List<WaterBlock> Water;

            /// <summary>
            /// Associates geometry with a specific material.
            /// </summary>
            [TagStructure(Size = 0x48, MaxVersion = CacheVersion.Halo2Vista)]
            [TagStructure(Size = 0x10, MinVersion = CacheVersion.Halo3Retail)]
            public class Part
            {
                [TagField(MaxVersion = CacheVersion.Halo2Vista)]
                public PartTypeOld TypeOld;

                [TagField(MaxVersion = CacheVersion.Halo2Vista)]
                public PartFlagsOld FlagsOld;

                /// <summary>
                /// The block index of the material of the mesh part.
                /// </summary>
                public short MaterialIndex;

                /// <summary>
                /// The transparent sorting index of the mesh part.
                /// </summary>
                [TagField(MinVersion = CacheVersion.Halo3Retail)]
                public short TransparentSortingIndex;

                /// <summary>
                /// The index of the first face vertex in the index buffer.
                /// </summary>
                public ushort FaceIndex;

                /// <summary>
                /// The number of faces in the part.
                /// </summary>
                public ushort FaceCount;

                /// <summary>
                /// The index of the first subpart that makes up this part.
                /// </summary>
                public short FirstSubPartIndex;

                /// <summary>
                /// The number of subparts that make up this part.
                /// </summary>
                public short SubPartCount;

                /// <summary>
                /// The type of the mesh part.
                /// </summary>
                [TagField(MinVersion = CacheVersion.Halo3Retail)]
                public PartTypeNew TypeNew;

                /// <summary>
                /// The flags of the mesh part.
                /// </summary>
                [TagField(MinVersion = CacheVersion.Halo3Retail)]
                public PartFlagsNew FlagsNew;

                [TagField(MaxVersion = CacheVersion.Halo2Vista)]
                public byte MaxNodesPerVertex;

                [TagField(MaxVersion = CacheVersion.Halo2Vista)]
                public byte ContributingCompoundNodeCount;

                /// <summary>
                /// The number of vertices that the mesh part uses.
                /// </summary>
                [TagField(MinVersion = CacheVersion.Halo3Retail)]
                public ushort VertexCount;

                [TagField(MaxVersion = CacheVersion.Halo2Vista)]
                public RealPoint3d Position;

                [TagField(Length = 4, MaxVersion = CacheVersion.Halo2Vista)]
                public byte[] NodeIndex = new byte[4];

                [TagField(Length = 3, MaxVersion = CacheVersion.Halo2Vista)]
                public float[] NodeWeight = new float[3];

                [TagField(MaxVersion = CacheVersion.Halo2Vista)]
                public float LodMipmapMagicNumber;

                [TagField(Padding = true, Length = 24, MaxVersion = CacheVersion.Halo2Vista)]
                public byte[] Unused = new byte[24];

                public enum PartTypeOld : short
                {
                    NotDrawn,
                    OpaqueShadowOnly,
                    OpaqueShadowCasting,
                    OpaqueNonshadowing,
                    Transparent,
                    LightmapOnly
                }

                [Flags]
                public enum PartFlagsOld : ushort
                {
                    None,
                    Decalable = 1 << 0,
                    NewPartTypes = 1 << 1,
                    DislikesPhotons = 1 << 2,
                    OverrideTriangleList = 1 << 3,
                    IgnoredByLightmapper = 1 << 4
                }

                public enum PartTypeNew : sbyte
                {
                    NotDrawn,
                    OpaqueShadowOnly,
                    OpaqueShadowCasting,
                    OpaqueNonshadowing,
                    Transparent,
                    LightmapOnly
                }

                [Flags]
                public enum PartFlagsNew : byte
                {
                    None,
                    IsWaterSurface = 1 << 0,
                    PerVertexLightmapPart = 1 << 1,
                    RenderInZPrepass = 1 << 2,
                    CanBeRenderedInDrawBundles = 1 << 3,
                    DrawCullDistanceMedium = 1 << 4,
                    DrawCullDistanceClose = 1 << 5,
                    DrawCullRenderingShields = 1 << 6,
                    DrawCullRenderingActiveCamo = 1 << 7
                }
            }

            /// <summary>
            /// A subpart of a mesh which can be rendered selectively.
            /// </summary>
            [TagStructure(Size = 0x8)]
            public class SubPart
            {
                /// <summary>
                /// The index of the first face vertex in the subpart.
                /// </summary>
                public ushort FaceIndex;

                /// <summary>
                /// The number of faces in the subpart.
                /// </summary>
                public ushort FaceCount;

                /// <summary>
                /// The index of the subpart visibility bounds.
                /// </summary>
                [TagField(MaxVersion = CacheVersion.Halo2Vista)]
                public short VisibilityBoundsIndex;

                /// <summary>
                /// The index of the part which this subpart belongs to.
                /// </summary>
                public short PartIndex;

                /// <summary>
                /// The number of vertices that the part uses.
                /// </summary>
                /// <remarks>
                /// Note that this actually seems to be unused. The value is pulled from
                /// the vertex buffer definition instead.
                /// </remarks>
                [TagField(MinVersion = CacheVersion.Halo3Retail)]
                public ushort VertexCount;
            }

            [TagStructure(Size = 0x14)]
            public class VisibilityBinding
            {
                public RealPoint3d Position;
                public float Radius;
                public byte NodeIndex;

                [TagField(Padding = true, Length = 3)]
                public byte[] Unused = new byte[3];
            }

            [TagStructure(Size = 0x44)]
            public class RawPoint
            {
                public RealPoint3d Position;

                [TagField(Length = 4)]
                public int[] NodeIndicesOld = new int[4];

                [TagField(Length = 4)]
                public float[] NodeWeights = new float[4];

                [TagField(Length = 4)]
                public int[] NodeIndices = new int[4];

                public int UseNewNodeIndices;
                public int AdjustedCompoundNodeIndex;
            }

            [TagStructure(Size = 0xC4)]
            public class RawVertex
            {
                public RawPoint Point = new RawPoint();
                public RealPoint2d Texcoord;
                public RealVector3d Normal;
                public RealVector3d Binormal;
                public RealVector3d Tangent;
                public RealVector3d AnisotropicBinormal;
                public RealPoint2d SecondaryTexcoord;
                public RealRgbColor PrimaryLightmapColor;
                public RealPoint2d PrimaryLightmapTexcoord;
                public RealVector3d PrimaryLightmapIncidentDirection;
                public RealRgbColor SecondaryLightmapColor;
                public RealPoint2d SecondaryLightmapTexcoord;
                public RealVector3d SecondaryLightmapIncidentDirection;
            }

            [TagStructure(Size = 0x2)]
            public class StripIndex
            {
                public short Index;
            }

            [TagStructure(Size = 0x20)]
            public class VertexBuffer
            {
                public byte TypeIndex;
                public byte StrideIndex;

                [TagField(Length = 30)]
                public byte[] Unknown = new byte[30];
            }

            [TagStructure(Size = 0x4)]
            public class RigidPointGroup
            {
                public byte RigidNodeIndex;
                public byte PointIndex;
                public short PointCount;
            }

            [TagStructure(Size = 0x2)]
            public class PointDataIndex
            {
                public short Index;
            }

            [TagStructure(Size = 0x1)]
            public class NodeMapping
            {
                public byte NodeIndex;
            }

            [TagStructure(Size = 0x10)]
            public class InstancedGeometryBlock
            {
                public short Section1;
                public short Section2;
                public List<ContentsBlock> Contents;

                [TagStructure(Size = 0x2)]
                public struct ContentsBlock
                {
                    public short Value;
                }
            }

            [TagStructure(Size = 0x2)]
            public struct WaterBlock
            {
                public short Value;
            }
        }

        [TagStructure(Size = 0x30)]
        public class BoundingSphere
        {
            public RealPlane3d Plane;
            public RealPoint3d Position;
            public float Radius;

            [TagField(Length = 4)]
            public sbyte[] NodeIndices;

            [TagField(Length = 3)]
            public float[] NodeWeights;
        }

        [TagStructure(Size = 0x18)]
        public class UnknownBlock
        {
            public byte UnknownByte1;
            public byte UnknownByte2;
            public short Unknown2;
            public byte[] Unknown3;
        }

        [TagStructure(Size = 0x20)]
        public class UnknownSection
        {
            [TagField(Align = 0x10)]
            public byte[] Unknown;

            public List<UnknownBlock> Unknown2;

            [TagStructure(Size = 0x2)]
            public struct UnknownBlock
            {
                public short Unknown;
            }
        }

        [TagStructure(Size = 0xC)]
        public struct PerMeshNodeMap
        {
            public List<NodeIndex> NodeIndices;

            [TagStructure(Size = 0x1)]
            public struct NodeIndex
            {
                public byte Node;
            }
        }

        [TagStructure(Size = 0xC)]
        public class PerMeshSubpartVisibilityBlock
        {
            public List<BoundingSphere> BoundingSpheres;
        }

        [TagStructure(Size = 0x10)]
        public class PerMeshLodDatum
        {
            public List<Index> Indices;

            public short VertexBufferIndex;

            [TagField(Padding = true, Length = 2)]
            public byte[] Unused;

            [TagStructure(Size = 0x4)]
            public struct Index
            {
                public int Value;
            }
        }

        /// <summary>
        /// A material describing how a mesh part should be rendered.
        /// </summary>
        [TagStructure(Size = 0x20, MaxVersion = CacheVersion.Halo2Vista)]
        [TagStructure(Size = 0x24, MaxVersion = CacheVersion.HaloOnline571627)]
        [TagStructure(Size = 0x30, MinVersion = CacheVersion.HaloOnline700123)]
        public class Material
        {
            /// <summary>
            /// The OLD render method tag to use to render the material.
            /// </summary>
            [TagField(Label = true, MaxVersion = CacheVersion.Halo2Vista)]
            public CachedTagInstance OldRenderMethod;

            /// <summary>
            /// The render method tag to use to render the material.
            /// </summary>
            [TagField(Label = true)]
            public CachedTagInstance RenderMethod;

            [TagField(MinVersion = CacheVersion.HaloOnline700123)]
            public List<Skin> Skins;
            public List<Property> Properties;
            public int Unknown;
            public sbyte BreakableSurfaceIndex;
            public sbyte Unknown2;
            public sbyte Unknown3;
            public sbyte Unknown4;

            [TagStructure(Size = 0x14)]
            public class Skin
            {
                [TagField(Label = true)]
                public StringId Name;
                public CachedTagInstance RenderMethod;
            }

            [TagStructure(Size = 0x2, MaxVersion = CacheVersion.Halo2Vista)]
            [TagStructure(Size = 0x4, MinVersion = CacheVersion.Halo3Retail)]
            public class PropertyType
            {
                [TagField(Label = true, MaxVersion = CacheVersion.Halo2Vista)]
                public Halo2Value Halo2;

                [TagField(Label = true, MinVersion = CacheVersion.Halo3Retail)]
                public Halo3Value Halo3;

                public enum Halo2Value : short
                {
                    LightmapResolution,
                    LightmapPower,
                    LightmapHalfLife,
                    LightmapDiffuseScale
                }

                public enum Halo3Value : int
                {
                    LightmapResolution,
                    LightmapPower,
                    LightmapHalfLife,
                    LightmapDiffuseScale
                }
            }

            [TagStructure(Size = 0x8, MaxVersion = CacheVersion.Halo2Vista)]
            [TagStructure(Size = 0xC, MinVersion = CacheVersion.Halo3Retail)]
            public class Property
            {
                public PropertyType Type;

                [TagField(MaxVersion = CacheVersion.Halo2Vista)]
                public short ShortValue;

                [TagField(MinVersion = CacheVersion.Halo3Retail)]
                public int IntValue;

                public float RealValue;
            }
        }
    }
}