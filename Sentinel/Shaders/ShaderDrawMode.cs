﻿using Sentinel.Serialization;

namespace Sentinel.Shaders
{
    [TagStructure(Size = 0x2)]
    public class ShaderDrawMode
    {
        public byte Index;
        public byte Count;
    }
}