﻿using System.Collections.Generic;
using Sentinel.Serialization;

namespace Sentinel.Shaders
{
    [TagStructure(Size = 0x50)]
    public class PixelShaderBlock
    {
        public byte[] Unknown;
        public byte[] PCShaderBytecode;
        public List<ShaderParameter> XboxParameters;
        public uint Unknown6;
        public List<ShaderParameter> PCParameters;
        public uint Unknown8;
        public uint Unknown9;
        public PixelShaderReference XboxShaderReference;
    }
}
