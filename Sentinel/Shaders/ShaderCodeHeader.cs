using Sentinel.Serialization;

namespace Sentinel.Shaders
{
    [TagStructure(Size = 0x8)]
    public class ShaderCodeHeader
    {
        public uint ConstantDataSize;
        public uint CodeDataSize;
    }
}
