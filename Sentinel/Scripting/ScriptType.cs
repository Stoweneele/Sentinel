namespace Sentinel.Scripting
{
    public enum ScriptType : short
    {
        Startup,
        Dormant,
        Continuous,
        Static,
        CommandScript,
        Stub
    }
}
