using Sentinel.Serialization;

namespace Sentinel.Scripting
{
    [TagStructure(Size = 0x28)]
    public class ScriptGlobal
    {
        [TagField(Length = 32)]
        public string Name;
        public ScriptValueType Type;
        public short Unknown;
        public uint InitializationExpressionHandle;
    }
}
