﻿namespace Sentinel.Audio
{
    public enum Compression : sbyte
    {
        PCM = 0x3,
        XMA = 0x7,
        MP3 = 0x8,
        FSB4 = 0x9
    }
}