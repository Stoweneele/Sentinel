﻿namespace Sentinel.Audio
{
    public enum ImportType : sbyte
    {
        Unknown,
        SingleShot,
        SingleLayer,
        MultiLayer
    }
}