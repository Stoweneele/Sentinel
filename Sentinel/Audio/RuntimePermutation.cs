﻿using Sentinel.Serialization;

namespace Sentinel.Audio
{
    [TagStructure(Size = 0x1)]
    public class RuntimePermutationFlag
    {
        public sbyte Unknown;
    }
}