﻿using Sentinel.Common;
using Sentinel.Serialization;


namespace Sentinel.Audio
{
    [TagStructure(Size = 0x4)]
    public class ImportName
    {
        public StringId Name;
    }
}