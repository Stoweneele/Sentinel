﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Sentinel.Cache;
using Sentinel.Tags.Definitions;
using Sentinel.Serialization;
using Sentinel.Common;
using Sentinel.Forms;
using System.Reflection;
using System.Collections;

namespace Sentinel.Controls
{
    public partial class StructMultiControl : UserControl, IFieldControl
    {
        public HaloOnlineCacheContext CacheContext { get; } = null;
        public CachedTagInstance TagInstance { get; } = null;
        public object Definition { get; } = null;
        public bool Loading { get; private set; } = false;
        
        public CacheForm Form { get; } = null;
        public Dictionary<string, IFieldControl> FieldControls { get; set; } = new Dictionary<string, IFieldControl>();

        public string TagName => CacheContext.TagNames.ContainsKey(TagInstance.Index) ? CacheContext.TagNames[TagInstance.Index] : $"0x{TagInstance.Index:X4}";

        public StructMultiControl()
        {
            InitializeComponent();
        }

        public StructMultiControl(CacheForm form, HaloOnlineCacheContext cacheContext, CachedTagInstance tagInstance, object definition) :
            this()
        {
            Form = form;
            CacheContext = cacheContext;
            TagInstance = tagInstance;
            Definition = definition;
        }

        protected override void OnLoad(EventArgs e)
        {
            if (treeView.Nodes.Count > 0)
                treeView.SelectedNode = treeView.Nodes[0];

            base.OnLoad(e);
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var field = treeView.SelectedNode.Tag as FieldInfo ?? null;

            splitContainer1.Panel2.Controls.Clear();
            FieldControls.Clear();
            FieldControls = CreateFieldControls(Form, splitContainer1.Panel2, field, Definition.GetType(), field == null);

            Loading = true;
            GetFieldValue(Definition, Definition, Definition);
            Loading = false;
        }

        public void GetFieldValue(object owner, object value = null, object definition = null)
        {
            var enumerator = new TagFieldEnumerator(new TagStructureInfo(value.GetType(), CacheContext.Version));

            if (!Loading)
            {
                var tagName = TagName;

                treeView.Nodes.Clear();
                treeView.Nodes.Add(new TreeNode
                {
                    Text = $"{(tagName.Contains('\\') ? tagName.Substring(tagName.LastIndexOf('\\') + 1) : tagName).ToPascalCase().ToSpaced()} ({value.GetType().Name.ToSpaced()})",
                    Tag = Definition
                });

                while (enumerator.Next())
                {
                    if (enumerator.Attribute.Padding == true)
                        continue;

                    var fieldType = enumerator.Field.FieldType;
                    var isTagBlock = (fieldType.IsGenericType && fieldType.GetGenericTypeDefinition() == typeof(List<>));
                    var isStruct = (fieldType.GetCustomAttributes(typeof(TagStructureAttribute), false).FirstOrDefault() as TagStructureAttribute ?? null) != null;

                    if (isTagBlock || isStruct)
                    {
                        var treeNode = new TreeNode
                        {
                            Text = enumerator.Field.Name.ToSpaced(),
                            Tag = enumerator.Field
                        };

                        if (isTagBlock)
                            treeNode.Text += $" ({((enumerator.Field.GetValue(value) as IList)?.Count ?? 0)})";

                        treeView.Nodes.Add(treeNode);
                    }
                }

                enumerator = new TagFieldEnumerator(new TagStructureInfo(value.GetType(), CacheContext.Version));
            }

            while (enumerator.Next())
                if (FieldControls.ContainsKey(enumerator.Field.Name))
                    FieldControls[enumerator.Field.Name].GetFieldValue(value, null, definition);
        }

        public void SetFieldValue(object owner, object value = null, object definition = null)
        {
            var enumerator = new TagFieldEnumerator(new TagStructureInfo(value.GetType(), CacheContext.Version));

            while (enumerator.Next())
                if (FieldControls.ContainsKey(enumerator.Field.Name))
                    FieldControls[enumerator.Field.Name].SetFieldValue(value);
        }

        private Dictionary<string, IFieldControl> CreateFieldControls(CacheForm form, Control parent, FieldInfo field, Type type, bool toplevel)
        {
            Enabled = false;

            var result = new Dictionary<string, IFieldControl>();
            var enumerator = new TagFieldEnumerator(new TagStructureInfo(type, CacheContext.Version));

            var currentLocation = new Point(parent is GroupBox ? 3 : 0, parent is GroupBox ? 16 : 0);

            while (enumerator.Next())
            {
                if (enumerator.Attribute.Padding == true)
                    continue;

                var fieldType = enumerator.Field.FieldType;
                var isFlagsEnum = (fieldType.GetCustomAttributes(typeof(FlagsAttribute), false).FirstOrDefault() as FlagsAttribute ?? null) != null;
                var isTagBlock = (fieldType.IsGenericType && fieldType.GetGenericTypeDefinition() == typeof(List<>));
                var isStruct = (fieldType.GetCustomAttributes(typeof(TagStructureAttribute), false).FirstOrDefault() as TagStructureAttribute ?? null) != null;
                var isBounds = (fieldType.IsGenericType && fieldType.GetGenericTypeDefinition() == typeof(Bounds<>));

                if (toplevel && (isStruct || isTagBlock))
                    continue;

                if (field != null && field != enumerator.Field)
                    continue;

                Control control = null;

                if (fieldType == typeof(StringId))
                {
                    control = new StringIdControl(CacheContext, enumerator.Field);
                }
                else if (fieldType.IsEnum)
                {
                    if (isFlagsEnum)
                    {
                        control = new FlagsControl(enumerator.Field);
                    }
                    else
                    {
                        control = new EnumControl(enumerator.Field);
                    }
                }
                else if (fieldType == typeof(Point2d))
                {
                    control = new Point2dControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(Rectangle2d))
                {
                    control = new Rectangle2dControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(RgbColor))
                {
                    control = new RgbColorControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(ArgbColor))
                {
                    control = new ArgbColorControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(RealPoint2d))
                {
                    control = new RealPoint2dControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(RealPoint3d))
                {
                    control = new RealPoint3dControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(RealVector2d))
                {
                    control = new RealVector2dControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(RealVector3d))
                {
                    control = new RealVector3dControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(RealQuaternion))
                {
                    control = new RealQuaternionControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(RealRgbColor))
                {
                    control = new RealRgbColorControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(RealArgbColor))
                {
                    control = new RealArgbColorControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(RealEulerAngles2d))
                {
                    control = new RealEulerAngles2dControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(RealEulerAngles3d))
                {
                    control = new RealEulerAngles3dControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(RealPlane2d))
                {
                    control = new RealPlane2dControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(RealPlane3d))
                {
                    control = new RealPlane3dControl(CacheContext, enumerator.Field);
                }
                else if (fieldType == typeof(CachedTagInstance))
                {
                    control = new TagReferenceControl(form, CacheContext, enumerator.Field);
                }
                else if (isTagBlock)
                {
                    control = new BlockControl(form, CacheContext, fieldType, enumerator.Field);

                    if (((BlockControl)control).Struct.FieldControls.Count == 0)
                        continue;
                }
                else if (isStruct)
                {
                    control = new StructControl(form, CacheContext, fieldType, enumerator.Field);

                    if (((StructControl)control).FieldControls.Count == 0)
                        continue;
                }
                else if (isBounds)
                {
                    control = new BoundsControl(CacheContext, enumerator.Field);
                }
                else
                {
                    control = new ValueControl(CacheContext, enumerator.Field);
                }

                control.Location = new Point(currentLocation.X, currentLocation.Y);
                parent.Controls.Add(control);

                currentLocation.Y = control.Bottom;

                if (parent is GroupBox gb && gb.AutoSize == false)
                {
                    parent.Width = control.Right + 8;
                    parent.Height = control.Bottom + 6;
                }

                if (isStruct || isTagBlock)
                    control.BringToFront();

                result[enumerator.Field.Name] = (IFieldControl)control;

                Application.DoEvents();
            }

            Enabled = true;

            return result;
        }
    }
}