﻿using System;
using System.Runtime.InteropServices;

namespace Sentinel.Common
{
    [StructLayout(LayoutKind.Sequential, Pack = 1, Size = 6)]
    public struct Half3
    {
        public Half X;
        public Half Y;
        public Half Z;
    }
}