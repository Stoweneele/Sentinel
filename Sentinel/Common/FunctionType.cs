namespace Sentinel.Common
{
    public enum FunctionType : short
    {
        Linear,
        Late,
        VeryLate,
        Early,
        VeryEarly,
        Cosine,
        Zero,
        One
    }
}
