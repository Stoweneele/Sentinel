﻿namespace Sentinel.Common
{
    public enum ResourceTypeGen2 : sbyte
    {
        TagBlock,
        TagData,
        VertexBuffer
    }
}