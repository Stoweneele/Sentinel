﻿using System;
using System.Runtime.InteropServices;

namespace Sentinel.Common
{
    [StructLayout(LayoutKind.Sequential, Pack = 1, Size = 8)]
    public struct Half4
    {
        public Half X;
        public Half Y;
        public Half Z;
        public Half W;
    }
}