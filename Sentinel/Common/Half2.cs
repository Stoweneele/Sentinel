﻿using System;
using System.Runtime.InteropServices;

namespace Sentinel.Common
{
    [StructLayout(LayoutKind.Sequential, Pack = 1, Size = 4)]
    public struct Half2
    {
        public Half X;
        public Half Y;
    }
}